var ROOT_PATH = "http://localhost/~argokasper/auth/";

function fbLogin() {
  console.log("tere");
  $.ajax({
    type: 'POST',
    url: 'fbconfig.php',
    success: function(data) {
      console.log(data);
    }
  });
}


function authenticateByIdCard() {
  $.ajax({
    type: "POST",
    headers: {"Access-Control-Allow-Origin": "https://localhost/~argokasper/auth/https", "Access-Control-Allow-Origin": "http://localhost/~argokasper/auth"},
    url: "https://localhost/~argokasper/auth/https/index.php",
    success: function(data) {
      alert(data);
      if(data.id) {
        loggedIn = true;
      } else {
        loggedIn = false;
      }
    },
    fail: function(error) {
      alert(error);
    }
  })
  /*var xhr = new XMLHttpRequest();
  xhr.open("GET", "https://localhost/~argokasper/auth/https/index.php", true);
  xhr.setRequestHeader("Access-Control-Allow-Origin", "http://localhost/~argokasper/auth/");
  xhr.onload = function () {
      console.log(xhr.responseText);
  };
  xhr.send();*/
}

function standardLogin() {
  var user = $(".loginpanel #user").val();
  var pass = $(".loginpanel #pwd").val();

  if(!user || !pass) {
    $(".loginpanel > .error").removeClass("hidden");
    return false;
  } else {
    $(".loginpanel > .error").addClass("hidden");

  }

  var loginForm = {
    username: user,
    password: pass
  }
  console.log(loginForm);
  $.ajax({
    type: "POST",
    url: "http://localhost/~argokasper/auth/DBactions.php",
    data: {loginData: loginForm},
    success: function(data) {
      console.log(data);
      var content = JSON.parse(data);
      if(content.status == "OK") {
        window.href = ROOT_PATH;
      } else {
        initializeForm();
        $(".loginpanel > .error").removeClass("hidden");
      }

    },
    fail: function(error) {
      alert(error);
    }
  });
}

function initializeForm() {
  $(".loginpanel > .error").addClass("hidden");
  $(".loginpanel > .pass > input").val("");
}

function resetForm() {
  $(".buttons > .back").addClass("hidden");
  $(".loginpanel input.login").val("Logi sisse").attr("onclick", "standardLogin(); return false;");
  $(".loginpanel .txt").last().remove();
  $(".loginpanel a.register").removeClass("hidden");

}

function openRegister() {
  initializeForm();
  if($(".loginpanel > .pass").length < 2) {
  $(".loginpanel > .pass").last().after('<div class="txt pass"><input id="pwd_again" type="password" placeholder="Parool uuesti" /><label for="pwd_again" class="entypo-lock"></label></div>');
  $(".loginpanel a.register").addClass("hidden");
  $(".loginpanel input.login").val("Registreeri").attr("onclick", "standardRegister(); return false;");
  $(".buttons").append('<a href="javascript:void()" class="back" onclick="resetForm(); return false;">Tagasi</a>');
  }
}

function standardRegister() {

  var user = $(".loginpanel #user").val();
  var pass = $(".loginpanel #pwd").val();
  var passAgain = $(".loginpanel #pwd_again").val();

  if(!user || !pass) {
    initializeForm();
    $(".loginpanel > .error").removeClass("hidden");
    return false;
  } else if(pass != passAgain) {
    initializeForm();
    $(".loginpanel > .error").removeClass("hidden");
    return false;
  } else {
    $(".loginpanel > .error").addClass("hidden");

  }

  var registerForm = {
    username: user,
    password: pass
  }
  console.log(registerForm);
  $.ajax({
    type: "POST",
    url: "http://localhost/~argokasper/auth/DBactions.php",
    data: {registerData: registerForm},
    success: function(data) {
      console.log(data);
      var content = JSON.parse(data);
      if(content.status == "OK") {
        resetForm();
      }


    },
    fail: function(error) {
      alert(error);
    }
  });
}



