<?php

namespace db;

class DB {

  private $host = "localhost";
  private $username = "root";
  private $password = "root";
  private $dbname = "test_prax2";

  private static $connection;

  /**
   * Connect to the database
   *
   * @return bool false on failure / mysqli PDO object instance on success
   */
  public function connect() {

    if(!isset(self::$connection)) {
      try {
        self::$connection = new \PDO("mysql:host=$this->host;dbname=$this->dbname", $this->username, $this->password);
        // set the PDO error mode to exception
        self::$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
      } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
      }
    }

    if(self::$connection === false) {
      return false;
    }

    return self::$connection;

  }

  public function insert($insertObj, $tableName) {
    $connection = $this->connect();

    foreach($insertObj as &$value) {
        $value = "'".$value."'";

    }

    $fields = implode(",", array_keys((array)$insertObj));
    $values = implode(",", array_values((array)$insertObj));

    $sql = "INSERT INTO $tableName ($fields) VALUES ($values)";
    $sth = $connection->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
    $result = $sth->execute();

    if ($result == 0) {
        throw new \Exception("Error: " . $sql . "</br>" . $connection->error);
    }

    return true;
  }

  public function update($insertObj, $conditions="", $tables="") {
    $connection = $this->connect();
    $updateData = array();

    foreach($insertObj as $field => $value) {
      $updateData[] = $field . "=" . $value;
    }

    $sql = "UPDATE $tables SET " . implode(", ", $updateData) . " WHERE $conditions";

    if ($connection->query($sql) === false) {
        throw new \Exception("Error: " . $sql . "<br>" . $connection->error);
    }

  }

  public function delete($conditions="", $tables="") {
    $connection = $this->connect();


    $sql = "DELETE FROM $tables WHERE $conditions";

    if ($connection->query($sql) === false) {
        throw new \Exception("Error: " . $sql . "<br>" . $connection->error);
    }
  }

  public function select($conditions, $fields="", $tables="") {
    $connection = $this->connect();
    $results = array();

    $sql = "SELECT " . (($fields != "") ? $fields : "*") . " FROM $tables " . (($conditions != "") ? "WHERE " . $conditions : "");
    $sth = $connection->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
    $sth->execute();
    $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
    if(is_array($result)) {
      foreach($result as $row) {
        $recData = new \StdClass();
        $recData->id = $row["user_id"];
        $recData->pic = $row["pic"];
        $recData->username = $row["username"];
        $recData->name = $row["name"];
        $recData->facebook = $row["fmail"];
        $recData->gmail = $row["gmail"];
        $recData->googleId = $row["google_id"];
        $results[] = $recData;
      }

    } else {
        return false;
    }
    return $results;

  }

}

//TESTDATA
//phpinfo();
/*
$test = new DB();
$obj = new \StdClass();
$obj->username = "aarx";
$obj->name = "argo käsper";
$obj->password = md5("kasper");
$test->insert($obj, "test_auth");
//print_r($test->select("","", "test_auth"));
//print_r($test->select("pic=123456789","pic", "test_auth"));
//$test->update($obj, "pic=$obj->pic", "test_auth");
//$test->delete("pic=$obj->pic", "test_auth");
*/


?>