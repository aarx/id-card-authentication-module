<?php
if(session_id()) {
  session_start();
}

include_once("config.php");
?>
<!DOCTYPE html>
<html xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
    <meta charset="UTF-8">
    <title>Different logins</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 </head>
  <body>
  <?php if ($_SESSION['user']): ?>      <!--  After user login  -->
<div class="wrapper">
  <div class="hero-unit">
    <h1>Hello <?php echo $_SESSION['USERNAME']; ?></h1>
    <p>Welcome to "facebook login" tutorial</p>
  </div>
  <div class="span4">
     <ul class="nav nav-list">
      <li class="nav-header">Image</li>
      <li><img src="https://graph.facebook.com/<?php echo $_SESSION['USERNAME']; ?>/picture"></li>
      <li class="nav-header">Facebook ID</li>
      <li><?php echo  $_SESSION['FBID']; ?></li>
      <li class="nav-header">Facebook fullname</li>
      <li><?php echo $_SESSION['FULLNAME']; ?></li>
      <div><a href="logout.php">Logout</a></div>
    </ul>
  </div>
</div>
    <?php else: ?>     <!-- Before login -->

        <div class="loginpanel">
          <div class="error hidden">Username and/or password is not entered and/or wrong</div>
          <div class="txt">
            <input id="user" type="text" placeholder="Kasutajanimi" />
            <label for="user" class="entypo-user"></label>
          </div>
          <div class="txt pass">
            <input id="pwd" type="password" placeholder="Parool" />
            <label for="pwd" class="entypo-lock"></label>
          </div>

          <div class="buttons">
            <input type="button" class="login" value="Logi sisse" onclick="standardLogin(); return false;"/>
            <span>
              <a href="javascript:void(0)" class="entypo-user-add register" onclick="openRegister(); return false;">Registreeru</a>
            </span>
          </div>

          <div class="hr">
            <div></div>
            <div>VÕI</div>
            <div></div>
          </div>

          <div class="social">
            <a href="javascript:void(0)" class="facebook" onclick="fbLogin();"></a>
            <a href="javascript:void(0)" class="googleplus"></a>
            <a href="javascript:void(0)" class="id" onclick="authenticateByIdCard();"></a>

          </div>
        </div>

    <?php endif ?>
    <style type="text/css">
      .hidden {
        display: none!important;
      }
      .error {
        color: red!important;

      }
    </style>
  </body>
</html>