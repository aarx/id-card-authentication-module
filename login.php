<?php

include_once("vendor/autoload.php");
date_default_timezone_set('UTC');
session_start();
$fb = new Facebook\Facebook([
  'app_id' => '159223834228980',
  'app_secret' => '9709640bd9a023fc77d358f8c2dd69b2',
  'default_graph_version' => 'v2.5',
  ]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email']; // Optional permissions
$loginUrl = $helper->getLoginUrl('http://localhost/~argokasper/auth/fb-callback.php', $permissions);

echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';