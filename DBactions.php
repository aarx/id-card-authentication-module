<?php
  include_once("db/DB.php");
  include_once("config.php");
  if(session_id()) {
    session_start();
  }

  if(isset($_POST["loginData"])) {
    $loginData = new StdClass();
    $loginData->username = $_POST["loginData"]["username"];
    $loginData->password = md5($_POST["loginData"]["password"]);

    $whereFrag = "username='$loginData->username' AND password='$loginData->password'";

    $db = new db\DB();
    $user = $db->select($whereFrag, "", "test_auth");

    if($user) {
      $_SESSION["user"] = $user;
      echo json_encode(array(
        "status" => "OK",
        "data"   => $user
        ));
    } else {
      echo json_encode(array(
        "status"    => "ERROR",
        "message"   => "Database error"
        ));
    }

  } else if(isset($_POST["registerData"])) {
    $registerData = new StdClass();
    $registerData->username = $_POST["registerData"]["username"];
    $registerData->password = md5($_POST["registerData"]["password"]);

    $db = new db\DB();
    $user = $db->insert($registerData, "test_auth");

    if($user) {
      $_SESSION["user"] = $user;
      echo json_encode(array(
        "status" => "OK",
        "data"   => $user
        ));

    } else {
      echo json_encode(array(
        "status"    => "ERROR",
        "message"   => "Database error"
        ));
    }

  } else {
    echo json_encode(array(
      "status"  => "ERROR",
      "message" => "Data not entered"
      ));
  }


?>